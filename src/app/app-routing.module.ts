import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListeEleveComponent } from './liste-eleve/liste-eleve.component';
import { ListeEtudiantComponent } from './liste-etudiant/liste-etudiant.component';
import { ListeParrainageComponent } from './liste-parrainage/liste-parrainage.component';
const routes: Routes = [
  //{path: 'home', component: PageAcceuilComponent},
  {path: '', redirectTo: 'home', pathMatch:'full'},
  {path: 'listeEtudiants', component: ListeEtudiantComponent},
  {path: 'listeEleves', component: ListeEleveComponent},
  {path: 'listeParrainage', component: ListeParrainageComponent},



 /* {     path: 'biens/:id', component: DetailBienComponent, 
        canActivate: [DetailBienGuard]
  },

  {path: 'add-user', component: AddUserComponent},

  {    path: 'biens/:id/edit', component: EditBienComponent, 
       canDeactivate: [BienEditGuard]
  },*/
  
  {path: '**', redirectTo: 'home', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
