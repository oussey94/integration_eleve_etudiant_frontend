import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import {HttpClientModule} from "@angular/common/http";

import { AppComponent } from './app.component';
import { ListeEtudiantComponent } from './liste-etudiant/liste-etudiant.component';
import { ListeEleveComponent } from './liste-eleve/liste-eleve.component';
import { ListeParrainageComponent } from './liste-parrainage/liste-parrainage.component';

@NgModule({
  declarations: [
    AppComponent,
    ListeEtudiantComponent,
    ListeEleveComponent,
    ListeParrainageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    //InMemoryWebApiModule.forFeature(EleveData),
    //InMemoryWebApiModule.forFeature(EtudiantData),
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
