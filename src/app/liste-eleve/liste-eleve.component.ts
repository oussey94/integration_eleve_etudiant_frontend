import { EleveService } from './../services/eleve.service';
import { Component, OnInit } from '@angular/core';
import { Eleve } from '../model/eleve.model';

@Component({
  selector: 'app-liste-eleve',
  templateUrl: './liste-eleve.component.html',
  styleUrls: ['./liste-eleve.component.css']
})
export class ListeEleveComponent implements OnInit {

  public title="Liste des eleves";

  public eleves: Eleve[];

  public errMsg: string;



  constructor(public eleveService: EleveService) { }

  ngOnInit(): void {
        this.eleveService.getEleves().subscribe({
          next: eleves =>{this.eleves=eleves;
          }, 
          error: err => this.errMsg=err
      });
  }

}
