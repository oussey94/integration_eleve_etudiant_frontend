import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Etudiant } from '../model/etudiant.model';
import { EtudiantService } from '../services/etudiant.service';

@Component({
  selector: 'app-liste-etudiant',
  templateUrl: './liste-etudiant.component.html',
  styleUrls: ['./liste-etudiant.component.css']
})
export class ListeEtudiantComponent implements OnInit {


  public title="Liste des etudiants";

  public etudiants: Etudiant[];

  public errMsg: string;

  currentEtudiant: any;
  selectedFiles;
  progress: number;
  currentFileUpload: any;
  public currentTime: number;
  public editPhoto: boolean;
  public mode: number = 0;


  constructor(public etudiantService: EtudiantService) { }

  ngOnInit(): void {
        this.etudiantService.getEtudiants().subscribe(
          {
            next: etudiants =>{this.etudiants=etudiants;
            }, 
            error: err => this.errMsg=err
        }
        );
  }

  onEditPhoto(e){
    this.currentEtudiant = e;
    this.editPhoto = true;
  }

  onSelectedFile(event){
    this.selectedFiles = event.target.files;
  }

  uploadPhoto(){
    this.progress = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.etudiantService.uploadPhotoEtudiant(this.currentFileUpload, this.currentEtudiant.idEtudiant).subscribe(event =>{
      if(event.type === HttpEventType.UploadProgress){
        this.progress = Math.round(100 * event.loaded / event.total);
      }else if(event instanceof HttpResponse){
        this.currentTime = Date.now()
        this.editPhoto = false;
      }
    }, err =>{
      alert("Problème de chargement !!!")
    });

    this.selectedFiles = undefined;
  }

  getTS(){
    return this.currentTime;
  }

}
