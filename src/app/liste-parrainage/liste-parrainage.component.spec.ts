import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeParrainageComponent } from './liste-parrainage.component';

describe('ListeParrainageComponent', () => {
  let component: ListeParrainageComponent;
  let fixture: ComponentFixture<ListeParrainageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeParrainageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeParrainageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
