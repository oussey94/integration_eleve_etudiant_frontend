import { Component, OnInit } from '@angular/core';
import { Eleve } from '../model/eleve.model';
import { Etudiant } from '../model/etudiant.model';
import { EleveService } from '../services/eleve.service';
import { EtudiantService } from '../services/etudiant.service';

@Component({
  selector: 'app-liste-parrainage',
  templateUrl: './liste-parrainage.component.html',
  styleUrls: ['./liste-parrainage.component.css']
})
export class ListeParrainageComponent implements OnInit {

  public title="Liste des parrainages";

  public eleves: Eleve[];

  public etudiants: Etudiant[];

  public errMsg: string;



  constructor(public eleveService: EleveService, public etudiantService: EtudiantService) { }

  ngOnInit(): void {
        this.eleveService.getEleve_parrain().subscribe({
          next: eleves =>{this.eleves=eleves;
          }, 
          error: err => this.errMsg=err
      });
  }
}
