import { Etudiant } from "./etudiant.model";

export class Eleve {
    idEleve: number;
    nomEleve: string;
    prenomEleve: string;
    photoEleve= 'assets/img/unknown.png';
    etablissementEleve: string;
    domaineEleve: string;
    etudiant: Etudiant;

}