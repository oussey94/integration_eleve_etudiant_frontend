import { Eleve } from "./eleve.model";

export class Etudiant {
    idEtudiant: number;
    nomEtudiant: string;
    prenomEtudiant: string;
    photoEtudiant: string;
    etablissementEtudiant: string;
    domaineEtudiant: string;
    amical: string;
    eleve: Eleve[];
}