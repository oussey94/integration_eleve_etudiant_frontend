import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Eleve } from '../model/eleve.model';

@Injectable({
  providedIn: 'root'
})
export class EleveService {

  public readonly eleves_url="http://localhost:9090/api1";

  public readonly eleves_parrains="http://localhost:9090/api1/parrainage";
  

  constructor(private http: HttpClient) { }

  public getEleves(): Observable <Eleve[]>{
    return this.http.get<Eleve[]>(this.eleves_url).pipe(
                          //tap(biens => console.log("biens: ", biens)),
      catchError(this.handleError)
    ); 
  }

  public getEleve_parrain(): Observable <Eleve[]>{
    return this.http.get<Eleve[]>(this.eleves_parrains).pipe(
                          //tap(biens => console.log("biens: ", biens)),
      catchError(this.handleError)
    ); 
  }

  public getEleveById(id: number): Observable<Eleve>{
    const url= `${this.eleves_url}/${id}`;

    return this.http.get<Eleve>(url).pipe(catchError(this.handleError));        //.getBiensImmo().pipe(map(biens => biens.find(bien => bien.id ==id)));
  }

  getFiles(id: number): Observable<Eleve> {
    const photoEtudiant= "photoEtudiant";
    return this.http.get<Eleve>(`${this.eleves_url}/${photoEtudiant}/${id}`);
  }

  public createEtudiant(eleve: Eleve): Observable<Eleve> {
    return this.http.post<Eleve>(this.eleves_url, eleve).pipe(catchError(this.handleError));
  }

  public updateEtudiant(eleve: Eleve): Observable<Eleve> {
    const url = `${this.eleves_url}/${eleve.idEleve}`;
    return this.http.put<Eleve>(url, eleve).pipe(catchError(this.handleError));
  }

  public deleteEtudiant(id: number): Observable<{}> {
    const url= `${this.eleves_url}/${id}`;
    return this.http.delete<{}>(url).pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }
  
}
