import { HttpClient, HttpErrorResponse, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { Etudiant } from '../model/etudiant.model';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EtudiantService {


  public readonly etudiants_url="http://localhost:9090/api2";

  constructor(private http: HttpClient) { }

  public getEtudiants(): Observable <Etudiant[]>{
    return this.http.get<Etudiant[]>(this.etudiants_url).pipe(
                          //tap(biens => console.log("biens: ", biens)),
      catchError(this.handleError)
    ); 
  }

  public getEtudiantById(id: number): Observable<Etudiant>{
    const url= `${this.etudiants_url}/${id}`;
    return this.http.get<Etudiant>(url).pipe(catchError(this.handleError));        //.getBiensImmo().pipe(map(biens => biens.find(bien => bien.id ==id)));
  }

  getFiles(id: number): Observable<Etudiant> {
    const photoEtudiant= "photoEtudiant";
    return this.http.get<Etudiant>(`${this.etudiants_url}/${photoEtudiant}/${id}`);
  }

  public createEtudiant(etudiant: Etudiant): Observable<Etudiant> {
    etudiant = {
      ...etudiant,
      photoEtudiant: 'assets/img/hotel-room.jpeg',
      idEtudiant: 0,
    }
    return this.http.post<Etudiant>(this.etudiants_url, etudiant).pipe(catchError(this.handleError));
  }

  public updateEtudiant(etudiant: Etudiant): Observable<Etudiant> {
    const url = `${this.etudiants_url}/${etudiant.idEtudiant}`;
    return this.http.put<Etudiant>(url, etudiant).pipe(catchError(this.handleError));
  }

  public deleteEtudiant(id: number): Observable<{}> {
    const url= `${this.etudiants_url}/${id}`;
    return this.http.delete<{}>(url).pipe(catchError(this.handleError));
  }


  uploadPhotoEtudiant(file: File, idEtudiant): Observable<HttpEvent<{}>> {
    let formData: FormData = new FormData();
    formData.append('file', file);
    const req = new HttpRequest('POST', this.etudiants_url+'/uploadPhoto/'+idEtudiant, formData, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

}
